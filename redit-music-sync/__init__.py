from logger import *
from resources import *
from syncman import *
from publish import *

logger = MaterialLogger()


def _usage():
    usage = """
    Usage: redit-music-sync [publish|sync|test]

    Redit Music Sync can be used in different modes as given above. 
    [publish] and [sync] can be used together
    """
    logger.log(usage.strip())
