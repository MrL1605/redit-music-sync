import sys

from syncman import *

logger = MaterialLogger()


def _usage():
    usage = """
    |Usage: redit-music-sync [publish|sync|config|test]
    |
    |Redit Music Sync can be used in different modes as given above. 
    |[publish] and [sync] can be used together
    """
    usage = map(lambda x: x.strip().strip("|"), usage.split("\n"))
    logger.log(usage)


if __name__ == '__main__':
    publish = False
    sync = False
    config = False
    if len(sys.argv) <= 1:
        _usage()
    for arg in sys.argv[1:]:
        if arg == 'publish':
            publish = True
        elif arg == 'sync':
            sync = True
        elif arg == 'config':
            config = True
        else:
            logger.fatal("Given argument [" + arg + "] is invalid.")
            _usage()

    if publish:
        Resources.read_config()
        Publisher().publish()
    if sync:
        Resources.read_config()
        Listener().start()
    if config:
        Resources.create_config()
