import mysql.connector
from logger import MaterialLogger
from mysql.connector import errorcode

logger = MaterialLogger()


class Connector:
    MODULE = 'MUSIC_MODULE'

    def __init__(self):
        self.ctx = None

    def connect(self, first_time=False):
        try:
            self.ctx = mysql.connector.connect(user='redit_dev', password='passwrd',
                                               host='localhost', database='redit_dev')

            self.ctx.autocommit = True
            if first_time:

                cursor = self.ctx.cursor(buffered=True)
                table_exists = """SELECT 1 FROM GLOBAL LIMIT 1"""
                try:
                    cursor.execute(table_exists)
                except mysql.connector.Error:
                    self.create_global_table(self.ctx)

                table_exists = """SELECT 1 FROM MUSIC_MODULE LIMIT 1"""
                try:
                    cursor.execute(table_exists)
                except mysql.connector.Error:
                    self.create_music_module_table(self.ctx)
                return cursor
            else:
                return self.ctx.cursor()
        except mysql.connector.Error as err:
            self.close()
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                logger.fatal("Something is wrong with user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                logger.fatal("Database does not exist")
            else:
                logger.error(err)

    @staticmethod
    def insert_files(cursor, files, current_rvn):
        for _file in files:
            params = []
            insert_query = "INSERT INTO MUSIC_MODULE ( file_path, rvn) VALUES (%s, %s)"
            params.append(_file)
            params.append(current_rvn)
            cursor.execute(insert_query, tuple(params))

    @staticmethod
    def delete_files(cursor, files):
        for _file in files:
            delete_query = "DELETE FROM MUSIC_MODULE WHERE file_path = %s"
            cursor.execute(delete_query, (_file,))

    @staticmethod
    def update_rvn(cursor, rvn=-1):
        update_query = """UPDATE GLOBAL SET rvn = """ + ("rvn + 1" if rvn == -1 else "%s") + """ WHERE module = %s"""
        cursor.execute(update_query, (Connector.MODULE,) if rvn == -1 else (Connector.MODULE, rvn))

    @staticmethod
    def get_rvn(cursor):
        select_query = """SELECT module, rvn FROM GLOBAL WHERE module = %s"""
        cursor.execute(select_query, (Connector.MODULE,))
        rvn = -1
        for (module, rvn) in cursor:
            pass
        return rvn

    @staticmethod
    def get_all_files(cursor):
        select_query = """SELECT file_path FROM MUSIC_MODULE"""
        cursor.execute(select_query)
        file_paths = []
        for file_path in cursor:
            file_paths.append(file_path[0])
        return file_paths

    @staticmethod
    def create_global_table(ctx):
        create_global_query = """CREATE TABLE GLOBAL (
            module VARCHAR(50) DEFAULT 'NULL' NULL,
            rvn INT NOT NULL,
            last_sync TIMESTAMP DEFAULT current_timestamp() NOT NULL,
            CONSTRAINT GLOBAL_module_uindex UNIQUE (module)
        )"""
        cursor = ctx.cursor()
        cursor.execute(create_global_query)
        insert_music_module = """INSERT INTO redit_dev.GLOBAL (module, rvn) VALUES (%s, 0)"""
        cursor.execute(insert_music_module, (Connector.MODULE,))

    @staticmethod
    def create_music_module_table(ctx):
        create_global_query = """CREATE TABLE MUSIC_MODULE (
            file_path VARCHAR(3071) DEFAULT '' NOT NULL
                PRIMARY KEY,
            rvn INT DEFAULT '0' NOT NULL,
            last_modified_on TIMESTAMP DEFAULT current_timestamp() NOT NULL,
            last_sync_on TIMESTAMP DEFAULT current_timestamp() NOT NULL,
            CONSTRAINT MUSIC_MODULE_file_path_uindex UNIQUE (file_path)
        )"""
        cursor = ctx.cursor()
        cursor.execute(create_global_query)

    def commit(self):
        if self.ctx is not None:
            self.ctx.commit()

    def close(self):
        if self.ctx is not None:
            self.ctx.close()
