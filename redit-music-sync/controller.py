import falcon
from connector import Connector
from syncman import SyncMan
from logger import MaterialLogger
from resources import Resources

logger = MaterialLogger()


class Index:
    def on_get(self, _, resp):
        logger.log("Requested is up")
        resp.content_type = falcon.MEDIA_HTML
        resp.body = """
        <html>
            <body>
                <h3>It works!</h3>
            </body>
        </html>
        """


class AllFiles:
    def on_get(self, _, resp):
        logger.log("Requested List of all files")
        con = Connector()
        cursor = con.connect(True)
        if cursor is None:
            resp.media = {"message": "Some Error while obtaining connection"}
        file_list = con.get_all_files(cursor)
        current_rvn = con.get_rvn(cursor)
        resp.media = {"files": file_list, "rvn": current_rvn}
        cursor.close()
        con.close()


class GetFile:
    def on_get(self, _, resp, name):
        logger.log("Requested File - ", name)
        resp.data = SyncMan.read_file(name)


Resources.read_config()
application = app = falcon.API()
app.add_route("/", Index())
app.add_route("/file", AllFiles())
app.add_route("/file/{name}", GetFile())
