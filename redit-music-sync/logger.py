import datetime as dt
import time


class BColors:
    def __init__(self):
        pass

    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class MaterialLogger:
    __END_L = '\x1b[0m'
    __FATAL = '\x1b[1;37;41m'
    __ERROR = '\x1b[1;31;40m'
    __LOG = '\x1b[0;30;47m'

    def __init__(self):
        pass

    @staticmethod
    def log(*message):
        if isinstance(message[0], list):
            message = map(str, message[0])
        else:
            message = map(str, message)
        message = "\n".join(message)
        message = dt.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + ": " + message
        print(MaterialLogger.__LOG + message + MaterialLogger.__END_L)

    @staticmethod
    def error(message):
        message = dt.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + ": " + message
        print(MaterialLogger.__ERROR + str(message) + MaterialLogger.__END_L)

    @staticmethod
    def fatal(message):
        message = dt.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + ": " + message
        print(MaterialLogger.__FATAL + str(message) + MaterialLogger.__END_L)
        raise Exception(message)
