import os
from connector import Connector
from logger import MaterialLogger
from resources import Resources

logger = MaterialLogger()


class Publisher:
    MUSIC_DIR = "/"
    connector = Connector()

    def __init__(self):
        self.MUSIC_DIR = Resources.MUSIC_DIR

    def publish(self):
        cursor = self.connector.connect(True)
        if cursor is None:
            logger.fatal("Could not connect to Database")
            return False

        # Get all the files current on this machine
        current_walk = os.walk(self.MUSIC_DIR)
        to_add = set([])
        to_delete = []
        for (path, _, file_names) in current_walk:
            map(lambda _f: to_add.add(path.split(self.MUSIC_DIR)[1] + _f), file_names)

        # Remove already published files
        files_list = self.connector.get_all_files(cursor)

        logger.log("Files in DB - ", files_list)
        for _file in files_list[:]:
            if _file in to_add:
                to_add.remove(_file)
            else:
                to_delete.append(_file)

        logger.log("Files to add to DB - ", to_add)
        # Start publishing if not empty
        if len(to_add) != 0:
            self.connector.insert_files(cursor, list(to_add), self.connector.get_rvn(cursor))
        logger.log("Files to delete from DB - ", to_delete)
        if len(to_delete) != 0:
            self.connector.delete_files(cursor, to_delete)
        cursor.close()
        self.connector.commit()
        self.connector.close()
        if len(to_add) == 0 and len(to_delete) == 0:
            logger.log("Publish Not required")
        else:
            logger.log("Publish Completed")

