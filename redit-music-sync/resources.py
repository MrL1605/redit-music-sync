import json
import os
from logger import MaterialLogger

logger = MaterialLogger()


class Resources:
    def __init__(self):
        pass

    DB_CONFIG = {}
    MUSIC_DIR = ""
    PEERS = []

    @staticmethod
    def read_config():
        try:
            with open("tmp/music-sync-resources.json") as json_config:
                config = json.load(json_config)
        except IOError:
            logger.fatal("Config not found. Please run setup.py")
            return
        if "database" not in config or "redit" not in config:
            logger.fatal("Invalid config. Please run setup.py")
            return
        if "type" not in config["database"] or "ip" not in config["database"] \
                or "user" not in config["database"] or "password" not in config["database"] \
                or "db" not in config["database"]:
            logger.fatal("Invalid config. Please run setup.py")
            return
        Resources.DB_CONFIG = config["database"]
        if "music-dir" not in config["redit"] or "peers" not in config["redit"]:
            logger.fatal("Invalid config. Please run setup.py")
            return
        Resources.MUSIC_DIR = config["redit"]["music-dir"]
        Resources.PEERS = config["redit"]["peers"]

    @staticmethod
    def create_config():
        Resources.DB_CONFIG["type"] = raw_input("Enter Database type: ")
        Resources.DB_CONFIG["ip"] = raw_input("Enter DB IP: ")
        Resources.DB_CONFIG["user"] = raw_input("Enter DB User: ")
        Resources.DB_CONFIG["password"] = raw_input("Enter DB Password: ")
        Resources.DB_CONFIG["db"] = raw_input("Enter DB Database: ")
        Resources.MUSIC_DIR = raw_input("Enter Directory to sync: ").strip().rstrip("/") + "/"
        print "Enter Peer complete HTTP address of peers in a new line each and an empty line when done:\n"
        peers = []
        line = "-"
        while line.strip() != "":
            line = raw_input().strip().rstrip("/")
            if line.strip() != "":
                peers.append(line + "/")
        Resources.PEERS = peers

        _f = file("tmp/music-sync-resources.json", "w")
        json.dump({
            "database": Resources.DB_CONFIG,
            "redit": {"music-dir": Resources.MUSIC_DIR, "peers": Resources.PEERS}
        }, _f, indent=2)
        _f.close()
