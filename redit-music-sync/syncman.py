import urllib
from threading import Thread
import json
import time
import os

import errno
from requests import ConnectionError
from logger import MaterialLogger
from connector import Connector
from publish import Publisher
from resources import Resources
import requests

logger = MaterialLogger()


class SyncMan:
    conn = Connector()

    def __init__(self):
        pass

    @staticmethod
    def read_file(path):
        logger.log("Reading file - ", Resources.MUSIC_DIR + path)
        return open(Resources.MUSIC_DIR + path, "r").read()

    @staticmethod
    def create_file(path, content):
        logger.log("Creating new file [" + path + "]")
        filename = Resources.MUSIC_DIR + path
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        with open(filename, "wb") as f:
            val = f.write(content)
        return val

    @staticmethod
    def sync_file(peer_ind, file_path):
        logger.log("Creating file [" + file_path + "]")
        url_file_path = urllib.quote(file_path, safe='')
        try:
            resp = requests.get(Resources.PEERS[peer_ind] + "/file/" + url_file_path)
            SyncMan.create_file(file_path, resp.content)
        except ConnectionError:
            pass

    @staticmethod
    def is_up(peer_ind):
        try:
            resp = requests.get(Resources.PEERS[peer_ind])
            if "It works" in resp.content:
                logger.log("Peer [" + Resources.PEERS[peer_ind] + "] is up")
                return True
        except ConnectionError:
            pass
        logger.error("Peer [" + Resources.PEERS[peer_ind] + "] is not up")
        return False

    def sync_peer(self, peer_ind):
        cursor = self.conn.connect(True)
        if cursor is None:
            return False
        my_files = self.conn.get_all_files(cursor)
        cursor.close()
        self.conn.close()
        logger.log("Starting Sync with [" + Resources.PEERS[peer_ind] + "]")
        resp = requests.get(Resources.PEERS[peer_ind] + "/file")
        peer_files = resp.json()["files"]
        to_publish = False
        for _f in peer_files:
            if _f not in my_files:
                to_publish = True
                self.sync_file(peer_ind, _f)
        logger.log("Sync with [", Resources.PEERS[peer_ind], "] completed")
        if to_publish:
            Publisher().publish()


class Listener(Thread):
    LAST_SYNC_ON = {}

    def run(self):
        delay = 1  # number of seconds to wait before next check
        logger.log("Starting Listener Thread")
        while True:
            if delay < 60:  # More then 15 mins, stop incrementing
                delay = delay + delay
            logger.log("Starting to check for peers")
            for ind in range(len(Resources.PEERS)):
                if Resources.PEERS[ind] not in self.LAST_SYNC_ON:
                    if SyncMan.is_up(ind):
                        SyncMan().sync_peer(ind)
                        self.LAST_SYNC_ON[Resources.PEERS[ind]] = time.time()
                    continue
                difference = time.time() - self.LAST_SYNC_ON[Resources.PEERS[ind]]
                if difference > 60 and SyncMan.is_up(ind):  # Next sync will only happen after 1 min
                    SyncMan().sync_peer(ind)
                    self.LAST_SYNC_ON[Resources.PEERS[ind]] = time.time()
            time.sleep(delay)
