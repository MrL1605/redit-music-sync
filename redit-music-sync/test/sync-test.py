from .. import *


def multiply(a, b):
    return a * b


def test_numbers_3_4():
    assert multiply(3, 4) == 12


def test_write_with_parent():
    try:
        os.removedirs("/tmp/py_test")
    except OSError:
        pass
    Resources.MUSIC_DIR = "/tmp/py_test/"
    SyncMan.create_file("there/one.txt", "Something in it")
    try:
        lines = open("/tmp/py_test/there/one.txt", "r").readlines()
        assert len(lines) == 1
        assert "Something in it" in lines[0]
    except OSError:
        assert False


if __name__ == '__main__':
    test_write_with_parent()


