from setuptools import setup

setup(
    name='redit_music_sync',
    version='1.2.4',
    description='A simple python script to maintain sync between different machines',
    author='Lalit Umbarkar',
    author_email='lalit.umbarkar9@gmail.com',
    packages=["redit-music-sync"],
    install_requires=['mysql', 'falcon', 'gunicorn', 'requests', 'datetime', 'pytest']
)
