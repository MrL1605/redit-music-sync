#!/usr/bin/env bash


check_and_kill() {

    [ -z "$1" ] || [ -z "$2" ] && err "Error: Incorrect usage" && return 1

    local pid_file=$1
    local cmd=$2

    [[ ! -f "${base_path}/tmp/${pid_file}" ]] && return 0
    local ppid=`cat "${base_path}/tmp/${pid_file}"`
    [[ -z `ps -p ${ppid} | grep ${cmd}` ]] && return 0

    echo "Cleaning up existing [${cmd}] process"
    kill -9 ${ppid}
}

resources_check() {

    if [ ! -f "${base_path}/tmp/music-sync-resources.json" ];then
        echo "Could not find the config. Please enter following details to create a config file, and then run start.sh again"
        python redit-music-sync config
    fi
}

main() {

    if [ "$1" = "publish" ]; then

        resources_check
        # Publish if there is an argument
        echo "Starting publish"
        python redit-music-sync publish > ./tmp/${publish_log_file} 2>&1 &
        echo "Logging in file [./tmp/${publish_log_file}]"

    elif [ "$1" = "stop" ]; then

        # Publish if there is an argument
        echo "Stopping server and SyncMan"
        check_and_kill syncman.pid "python"
        check_and_kill gunicorn.pid "gunicorn"
    else

        # Else create the gunicorn server try listening
        # Create logs base directory
        mkdir -p ${base_path}/tmp

        resources_check

        check_and_kill syncman.pid "python"
        check_and_kill gunicorn.pid "gunicorn"

        echo "Starting SyncMan"
        python redit-music-sync sync > ./tmp/${syncman_log_file} 2>&1 &
        echo $! > tmp/syncman.pid
        echo "Logging in file [./tmp/${syncman_log_file}]"
        echo "Starting Gunicorn server"
        gunicorn -b 0.0.0.0:54545 redit-music-sync.controller:app > ./tmp/${gunicorn_log_file} 2>&1 &
        echo $! > tmp/gunicorn.pid
        echo "Logging in file [./tmp/${gunicorn_log_file}]"
    fi

}


# Get to basePath
base_path=`dirname $0`
cwd=`pwd`
cd ${cwd}
cd ${base_path}
base_path=`pwd`

# Log file names
gunicorn_log_file=gunicorn-`date +%d%m%y%H%M%S`.log
syncman_log_file=syncman-`date +%d%m%y%H%M%S`.log
publish_log_file=publish-`date +%d%m%y%H%M%S`.log


main $*
